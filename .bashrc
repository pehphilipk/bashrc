#LARBS#
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias grep='grep --color=auto'
PS1='[\u@\h \W]\$ '
#LARBS#

#cc cmd
alias cpb='for file in *.*; do cp "$file" "${file%.*}_blur.png"; done'

#open projects
alias projex='cd C:\Programming && explorer.exe .'
alias proj='cd C:\Programming'

#npm
alias ni='npm i'
alias nr='npm run'

#utility cmd
alias ex='explorer.exe'
alias bashrc='vim ~/.bashrc'
alias sbrc='source ~/.bashrc'


#git
alias gitrs='git reset --soft head^' 
alias gitrhard='git reset --hard head^' 
alias gitss='git stash save^' 
alias gitbdelete='git branch | grep -v "main" | xargs git branch -D' 
#savefilenames
sfn() {
	find . -type f > filenames.txt
}


#find & copy all pictures out
cpimg() {
	if [ "$#" -ne 2 ]; then
		echo "Usage: copy_images <source_folder> <output_folder>"
		return 1
	fi

	source_folder="$1"
	output_folder="$2"

  # Create the output folder if it doesn't exist
  mkdir -p "$output_folder"

  # Find all .png and .jpg files in the source folder and its subfolders
  find "$source_folder" -type f \( -iname "*.png" -o -iname "*.jpg" \) -exec cp {} "$output_folder" \;
}


#mfos
mfos() {
	find . -mindepth 2 -type f -exec mv {} . \;
	find . -mindepth 1 -type d -empty -delete
}


rf() {
	if [[ $# -lt 2 || $# -gt 3 ]]; then
		echo "Usage: rf_recursive [directory] <old_keyword> <new_keyword>"
		return 1
	fi

	if [[ $# -eq 2 ]]; then
		directory="."
		old_keyword="$1"
		new_keyword="$2"
	else
		directory="$1"
		old_keyword="$2"
		new_keyword="$3"
	fi

	if [[ ! -d "$directory" ]]; then
		echo "Error: Directory '$directory' does not exist."
		return 1
	fi

	# Find and rename files recursively
	find "$directory" -type f -name "*$old_keyword*" | while read -r file; do
		file_basename=$(basename "$file")
		new_name="${file_basename//$old_keyword/$new_keyword}"
		new_path="${file%/*}/$new_name"
		
		if [[ "$file" != "$new_path" ]]; then
			mv "$file" "$new_path"
			echo "Renamed '$file_basename' to '$new_name'"
		fi
	done
}

# Example usage:
# rf_recursive old_text new_text
# rf_recursive /path/to/directory old_text new_text

#Delete files
df() {
	if [[ $# -lt 1 ]]; then
		echo "Usage: delete_files [-f] <extension/keyword>"
		return 1
	fi

	local force_flag=""
	if [[ $1 == "-f" ]]; then
		force_flag="-f"
		shift
	fi

	local pattern="$1"

	if [[ $force_flag == "-f" ]]; then
		files=()
		if [[ -n "$pattern" && -z "$(echo "$pattern" | grep '\.')" ]]; then
			# No dot in pattern, treat as a keyword
			files=($(ls | grep "$pattern"))
		else
			# Pattern contains a dot, treat as an extension
			files=(*."$pattern")
		fi

		if [[ ${#files[@]} -gt 0 ]]; then
			for file in "${files[@]}"; do
				rm $force_flag "$file"
				echo "Deleted '$file'"
			done
		else
			echo "No files matching '$pattern' found."
		fi
	else
		read -p "Are you sure you want to delete all files matching '$pattern'? [y/N]: " confirmation

		if [[ $confirmation =~ ^[Yy]$ ]]; then
			df -f "$pattern"
		else
			echo "Deletion canceled."
		fi
	fi
}

# RenameFilesKeyWord
function rfk() {
	# Check if the number of arguments is correct
	if [ $# -ne 2 ]; then
		echo "Usage: rename_files OLD NEW"
		return 1
	fi

    # Assign the arguments to variables
    OLD="$1"
    NEW="$2"

    # Recursive function to rename files and directories
    recursive_rename() {
	    for file in "$1"/*; do
		    if [ -f "$file" ] || [ -d "$file" ]; then
			    # Get the directory path and filename of the file
			    dir="$(dirname "$file")"
			    filename="$(basename "$file")"

		# Replace OLD with NEW in the directory path and filename
		new_dir="$(echo "$dir" | sed "s/$OLD/$NEW/g")"
		new_filename="$(echo "$filename" | sed "s/$OLD/$NEW/g")"

		# Rename the file or directory
		if [ "$dir/$filename" != "$new_dir/$new_filename" ]; then
			mv "$dir/$filename" "$new_dir/$new_filename"
			echo "Renamed '$dir/$filename' to '$new_dir/$new_filename'"
		fi

		# Recursively call the function for directories
		if [ -d "$new_dir/$new_filename" ]; then
			recursive_rename "$new_dir/$new_filename"
		fi
		    fi
	    done
    }

    # Call the recursive function for the current directory
    recursive_rename .
}
ri(){
	if [ $# -ne 2 ]; then
		echo "Usage: resize_images <source_folder> <dimensions>"
		return 1
	fi

	source_folder="$1"
	dimensions="$2"
	parent_folder="$(dirname "$source_folder")"
	folder_name="$(basename "$source_folder")"
	output_folder="${parent_folder}/${folder_name}_resized"

	mkdir -p "$output_folder"

	for img_file in "$source_folder"/*; do
		if [ -f "$img_file" ]; then
			output_file="$output_folder/$(basename "$img_file")"
			magick convert "$img_file" -resize "$dimensions" "$output_file"
		fi
	done
}
